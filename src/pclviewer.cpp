#include "pclviewer.h"
#include "ui_pclviewer.h"

PCLViewer::PCLViewer (QWidget *parent) :
  QMainWindow (parent),
  ui (new Ui::PCLViewer)
{
  ui->setupUi (this);
  this->setWindowTitle ("LUM Registration Testbed");

  // Set up the QVTK window
  viewer.reset (new pcl::visualization::PCLVisualizer ("viewer", false));
  ui->qvtkWidget->SetRenderWindow (viewer->getRenderWindow ());
  viewer->setupInteractor (ui->qvtkWidget->GetInteractor (), ui->qvtkWidget->GetRenderWindow ());
  ui->qvtkWidget->update ();

  // connect load button
  connect (ui->loadPCButton, SIGNAL (clicked()), this, SLOT (loadPointClouds ()));
  // connect visualization toggle buttons (each for input clouds resp. transformed clouds)
  connect (ui->showInputBtn, SIGNAL (clicked()), this, SLOT (showInputCloudsClicked ()));
  connect (ui->showOutputBtn, SIGNAL (clicked()), this, SLOT (showTransformedCloudsClicked ()));
  connect (ui->startLumBtn, SIGNAL (clicked()), this, SLOT (performLumRegistration ()));

}

void PCLViewer::loadPointClouds() {
    // open file dialog in the relative ted directory
    QString tedPath = QDir::currentPath() + QDir::separator() + ".." + QDir::separator() + "ted";
    QStringList filenames = QFileDialog::getOpenFileNames(this,tr("PCD files"), tedPath,tr("PCD files (*.pcd)") );
    if( !filenames.isEmpty() )
    {
        //flush previous input
        input_clouds.clear();
        input_clouds_col.clear();

        for (int i =0;i<filenames.count();i++) {
            std::string fname_str = filenames.at(i).toLocal8Bit().constData();
            //store plain point clouds for lum algorithm
            CloudPtr pc (new Cloud);
            pcl::io::loadPCDFile (fname_str, *pc);
            input_clouds.push_back (CloudPair (fname_str, pc));

            //store colored point clouds for visualization
            CloudCol::Ptr pc_col(new CloudCol);
            copyPointCloud(*pc, *pc_col);
            input_clouds_col.push_back(CloudColPair (fname_str, pc_col));
        }
        printf("%i point clouds added \n", filenames.count());

        //visualize loaded point clouds
        visualizePointClouds();

        // add point clouds to lum instance
        addCloudsToLum();
    }
}

void PCLViewer::visualizePointClouds() {
    //flush visualization
    viewer->removeAllPointClouds();
    for (size_t i = 0; i < input_clouds_col.size (); i++) {
        //apply random color
        uint r = 255 *(1024 * rand () / (RAND_MAX + 1.0f));
        uint g = 255 *(1024 * rand () / (RAND_MAX + 1.0f));
        uint b = 255 *(1024 * rand () / (RAND_MAX + 1.0f));
        CloudColPtr pc = input_clouds_col[i].second;
        for (size_t i = 0; i < pc->size (); i++)
          {
            pc->points[i].r = r;
            pc->points[i].g = g;
            pc->points[i].b = b;
          }
        //add to visualizer
        viewer->addPointCloud (pc, input_clouds_col[i].first);
    }
    //update visualization
    viewer->resetCamera ();
    ui->qvtkWidget->update ();
}

void PCLViewer::addCloudsToLum() {
    // clear list-widget of input clouds
    ui->inputPointCloudList->clear();

    // add newly loaded point clouds
    for (size_t i = 0; i < input_clouds.size (); i++) {
        auto *item = new QListWidgetItem();
        // get filepath from stored cloud pair to name list entry
        std::string filepath = input_clouds[i].first;
        // check if cloud contained in ted-directory, if so apply subsequent filepath, otherwise apply full path
        size_t checkTed = filepath.find("ted");
        std::string listEntry = checkTed == std::string::npos ? filepath : filepath.substr(filepath.find("ted"));
        // add resulting point cloud path to list of input clouds
        item->setText(QString::number(i).rightJustified(2, '0') + ":  " + QString::fromStdString(listEntry));
        ui->inputPointCloudList->addItem(item);

    }
}

void PCLViewer::showInputCloudsClicked() {
    ui->showOutputBtn->setChecked(false);
}

void PCLViewer::showTransformedCloudsClicked() {
    ui->showInputBtn->setChecked(false);
}

void PCLViewer::performLumRegistration() {
    printf("performing lum registration algorithm \n");
}

PCLViewer::~PCLViewer ()
{
  delete ui;
}
