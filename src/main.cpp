#include "pclviewer.h"
#include <QApplication>
#include <QMainWindow>
#include <QDesktopWidget>

int main (int argc, char *argv[])
{
  QApplication a (argc, argv);
  PCLViewer w;

  w.showMaximized();

  return a.exec ();
}
