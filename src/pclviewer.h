#ifndef PCLVIEWER_H
#define PCLVIEWER_H

#include <iostream>

// Qt
#include <QMainWindow>
#include <QFileDialog>

// Point Cloud Library
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/io/pcd_io.h>

// Visualization Toolkit (VTK)
#include <vtkRenderWindow.h>

// std
#include <iostream>
#include <string>

#include <vector>

// simplify standard pcl classes
typedef pcl::PointXYZ PointType;
typedef pcl::PointCloud<PointType> Cloud;
typedef Cloud::ConstPtr CloudConstPtr;
typedef Cloud::Ptr CloudPtr;
typedef std::pair<std::string, CloudPtr> CloudPair;
typedef std::vector<CloudPair> CloudVector;

// same for colored clouds
typedef pcl::PointXYZRGB PointTypeCol;
typedef pcl::PointCloud<PointTypeCol> CloudCol;
typedef CloudCol::Ptr CloudColPtr;
typedef std::pair<std::string, CloudColPtr> CloudColPair;
typedef std::vector<CloudColPair> CloudColVector;

typedef unsigned int uint;

namespace Ui
{
  class PCLViewer;
}

class PCLViewer : public QMainWindow
{
  Q_OBJECT

public:
  explicit PCLViewer (QWidget *parent = 0);
  ~PCLViewer ();

public Q_SLOTS:

private Q_SLOTS:
    /*!
     * Opens file dialog to select input point clouds as .pcd format
     */
    void
    loadPointClouds();

    /*!
     * Add loaded point clouds to lum slam-graph
     */
    void
    addCloudsToLum();

    /*!
     * Toggle visualization to show the raw input point clouds
     */
    void
    showInputCloudsClicked();

    /*!
     * Toggle visualization to show the transformed output point clouds
     */
    void
    showTransformedCloudsClicked();

    /*!
     * Applies the lum registration algorithm to the loaded point clouds
     */
    void
    performLumRegistration();



protected:
    void
    visualizePointClouds();

  boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer; /// instance of 3d visualizer
  CloudVector input_clouds; /// contains the initially loaded raw point clouds
  CloudColVector input_clouds_col; /// contains colored input point clouds for visualization

private:
  Ui::PCLViewer *ui;

};

#endif // PCLVIEWER_H
